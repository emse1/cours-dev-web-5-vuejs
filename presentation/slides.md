# Frontend Web Development - VueJS

Quentin Richaud

qrichaud.pro@gmail.com

---

# JS Frontend Frameworks

In the previous lecture, we saw some JS libraries (JQuery, lodash, axios…).

Now we are going to see JS frameworks. What is the difference between a library and a framework?

- **Library** : consists of functions that an application can call to perform a task.
- **Framework** : defines how a developer designs an application. In other words, the framework calls on the application code, rather than the other way around. 

![](./imgs/framework_vs_library1.png)

---

# JS Frontend Frameworks


![](./imgs/framework_vs_library2.png)

---

# Ecosystem of JS frameworks

Today 3 main frontend frameworks : Angular, React, VueJS.

Other rising frameworks : NextJS (using React), Nuxt (based on Vue), Svelte, …

The JS ecosystem is evolving (too) fast : in a year or two, maybe new frameworks will come in the spotlight.

---

# Angular

- Developped by google
- Not to be confused with AngularJS (developped by the same team, but older, now deprecated framework)
- Full fledged framework : provides an opiniated way to code everything in your app
- More complex, thus a bit hearder to learn
- More robust for big projects, enterprise projects
- Uses Typescript
- Uses a templating system (declarative paradigm)

---

# React

- Developped by facebook
- In itself, is just a rendering library
- Can be coupled with many tool to become "framework" like
- Many different ways of configuring a React project (so very different project architectures from one projet to another)
- Doesn't use templates, (imperative paradigm)
- Biggest share of the market

---

# Vue

- Looks like a lightweight Angular
- Uses a templating system (declarative paradigm)
- Provides less tooling by default than Angular (more modularity)
- More flexible and easy to setup into small projects


---

1. Explaining that  Vue provides easy way to bind data in your JS code and DOM elements

2. Show the code of a simple component, illustate one way data binding. 
Then two ways data binding

3. Explain that for it to work, the code is compiled by a NodeJS toolchain, into regularJS for the web browser, the framework does that for you

4. Step by step following the official guide

Steps to highlight in the official guide : 

1st example

  a. Creating an app (linking app to HTML)
  b. Text interpolation and attribute binding (shorthand syntax only)
  c. Calling functions into text interpolation


2nd example

  a. directives v-if
  b. v-on directive

3rd example

  a. class binding

4th example

  a. v-for

No example

  a. event handling, show details on vue Guide

5th example 

  a. v-model on a input
  b. show quickly all type of inputs on the vue Guide

6th example

  a. dividing code into subcomponents
  b. defining a component (new .vue file)
  c. importing the component in the parent to use it
  d. show that each component has its independant state

7th example

  a. component props : passing data from the parent to a child component

8th example

  b. component emits : passing data up from a child component to its parent
  c. explain the names case convention : camelCase et kebab-case

9th example

  a. Lifecycle hooks
  b. For Ajax : it's not Vue related code, juste do Ajax as you did with Vanilly JS

---

# VueJS purpose

VueJS provides tools that make it really easy to bind data (in your JS code) and the DOM.

Displaying data in DOM element, updating data from DOM input elements, binding actions
to DOM events.

All of this takes several lines of code in pure JS (as you experienced with your last
assignment). VueJS provides a lot of tools, and some "magic" thanks to its preprocessors
(that are written in NodeJS), in order to make all this linking between data and DOM
really fast for the programmer.

---

# VueJS Documentation & code examples

The VueJS official documentation is excellent, and I'm going to directly quote it.

For many programming tools (languages, librairies, frameworks, programs), the official documentations
and guides are almost always the best place to start.

Link to the doc : https://vuejs.org/guide/introduction.html

You will find in the lecture repository, a list of code examples, to see each principle
put into action.

---

# Running each example

1. Go to the example directory (e.g `/demo_code/1_Basic_Vue_JS_Component`)
2. Install the dependencies with `npm install`
3. Run the frontend server with `npm run dev`

The frontend is now server on http://localhost:5173

Each demo folder is a whole VueJS project, so you need to install the dependencies for each example.

The projects are setup with a lot of NodeJS tools (that are provided to you out-of-the-box).
Setting up such tools are outside the scope of this lecture, however it's good to understand
what's going on under the hood.


---

# Example 1

## The application instance

Every Vue application starts by creating a new **application instance** with the [`createApp`](/api/application#createapp) function:

```js
import { createApp } from 'vue'
// import the root component App from a single-file component.
import App from './App.vue'

const app = createApp(App)
```

- This is done in the `main.js` file, that is imported in the webpage HTML document and thus run by the browser
- The `App` import is a VueJS component, defined in source file with a specific VueJS syntax : `App.vue`
- These imports, and specific source formats are compiled by the framework toolchain (composed of several NodeJS programs) and transformed into regular JS, that will be sent to the browser

---


## Mounting the App

An application instance won't render anything until its `.mount()` method is called. It expects a "container" argument, which can either be an actual DOM element or a selector string:

```html
<div id="app"></div>
```

```js
app.mount('#app')
```

The content of the app's root component will be rendered inside the container element. The container element itself is not considered part of the app.

The `.mount()` method should always be called after all app configurations and asset registrations are done. Also note that its return value, unlike the asset registration methods, is the root component instance instead of the application instance.

---

# What is a Vue Component


Vue works with "components". A Component is small reusable part of the HTML interface, coupled with JS logic.

A Component is a HTML-like template (HTML enhanced with directives to write declarative logic) coupled with a JS object
that contains data and functions, that works with the template.

Once a component is developped, we can use it in other components with a novel HTML tag. For example, I make a component
named `MyComponent` : I can include it in the HTML template of other Vue components with `<my-component></my-component>`.

Thue VueJS app bootstrap itself by including the root Vue component of your application into a DOM node (see `main.js`). 
Then all the application is rendered by the tree of Vue components included from your root component.

---

# Vue components

![](./imgs/vue_components1.png)

---

# Vue components

![](./imgs/vue_components2.png)

---

All rendering is done by VueJS (that is frontend logic). You can see that the initial HTML document is quite empty.

---

---

# Vue Component Syntax


```vue
<script setup>
import { ref } from 'vue'

const noteTitle = ref("Hello there");

function generateContent(length) {
  return "bla ".repeat(length);
}
</script>

<template>
  <div class="notes-container">
    <div class="note" :id="noteId">
      <h2>{{ noteTitle }}</h2>
      <p>{{ generateContent(6) }}</p>
    </div>
  </div>
</template>


<style lang="scss" scoped>
.notes-container {
  display: flex;
  flex-wrap: wrap;
}
</style>

```

---

# Vue Component Syntax

We can see 3 elements in this Vue file

1. The JS script
2. The pseudo-HTML template
3. The scoped stylesheet (in this case SCSS)

Let's see how the Vue template works, in order to bind with the JS data.

---

## Text Interpolation 

The most basic form of data binding is text interpolation using the "Mustache" syntax (double curly braces):

```vue-html
<span>Message: {{ msg }}</span>
```

The mustache tag will be replaced with the value of the `msg` property [from the corresponding component instance](/guide/essentials/reactivity-fundamentals#declaring-reactive-state). It will also be updated whenever the `msg` property changes.

---

## Attribute Bindings

Mustaches cannot be used inside HTML attributes. Instead, use a [`v-bind` directive](/api/built-in-directives#v-bind):

```vue-html
<div v-bind:id="dynamicId"></div>
```

The `v-bind` directive instructs Vue to keep the element's `id` attribute in sync with the component's `dynamicId` property. If the bound value is `null` or `undefined`, then the attribute will be removed from the rendered element.

---

### Shorthand 

Because `v-bind` is so commonly used, it has a dedicated shorthand syntax:

```vue-html
<div :id="dynamicId"></div>
```

Attributes that start with `:` may look a bit different from normal HTML, but it is in fact a valid character for attribute names and all Vue-supported browsers can parse it correctly. In addition, they do not appear in the final rendered markup. The shorthand syntax is optional, but you will likely appreciate it when you learn more about its usage later.

> In the examples, I will be using the shorthand syntax in code examples, as that's the most common usage for Vue developers.


---

### Calling Functions 

It is possible to call a component-exposed method inside a binding expression:

```vue-html
<time :title="toTitleDate(date)" :datetime="date">
  {{ formatDate(date) }}
</time>
```

#### Tip
Functions called inside binding expressions will be called every time the component updates, so they should **not** have any side effects, such as changing data or triggering asynchronous operations.

---

# See Code Example 1 

These functionalities are put into practice in Code Example 1 :

- creating and mounting an 
- discovering the Vue Component file syntax
- discovering the syntax of Vue templates :
  - text interpolation
  - attribute binding
  

---

## Directives 

Directives are special attributes with the `v-` prefix. Vue provides a number of [built-in directives](/api/built-in-directives), including `v-html` and `v-bind` which we have introduced above.

Directive attribute values are expected to be single JavaScript expressions (with the exception of `v-for`, `v-on` and `v-slot`, which will be discussed in their respective sections later). A directive's job is to reactively apply updates to the DOM when the value of its expression changes. Take [`v-if`](/api/built-in-directives#v-if) as an example:

```vue-html
<p v-if="seen">Now you see me</p>
```

Here, the `v-if` directive would remove or insert the `<p>` element based on the truthiness of the value of the expression `seen`.

---

## Listening to Events

We can use the `v-on` directive, which we typically shorten to the `@` symbol, to listen to DOM events and run some JavaScript when they're triggered. The usage would be `v-on:click="handler"` or with the shortcut, `@click="handler"`.

The handler value can be one of the following:

1. **Inline handlers:** Inline JavaScript to be executed when the event is triggered (similar to the native `onclick` attribute).

2. **Method handlers:** A property name or path that points to a method defined on the component.

---

## Inline Handlers 

Inline handlers are typically used in simple cases, for example:



```js
const count = ref(0)
```




```vue-html
<button @click="count++">Add 1</button>
<p>Count is: {{ count }}</p>
```

---

## Method Handlers

The logic for many event handlers will be more complex though, and likely isn't feasible with inline handlers. That's why `v-on` can also accept the name or path of a component method you'd like to call.

For example:


```js
const name = ref('Vue.js')

function greet(event) {
  alert(`Hello ${name.value}!`)
  // `event` is the native DOM event
  if (event) {
    alert(event.target.tagName)
  }
}
```


```vue-html
<!-- `greet` is the name of the method defined above -->
<button @click="greet">Greet</button>
```

---

## Calling Methods in Inline Handlers 

Instead of binding directly to a method name, we can also call methods in an inline handler. This allows us to pass the method custom arguments instead of the native event:


```js
function say(message) {
  alert(message)
}
```


```vue-html
<button @click="say('hello')">Say hello</button>
<button @click="say('bye')">Say bye</button>
```

---

## Accessing Event Argument in Inline Handlers

Sometimes we also need to access the original DOM event in an inline handler. You can pass it into a method using the special `$event` variable, or use an inline arrow function:

```vue-html
<!-- using $event special variable -->
<button @click="warn('Form cannot be submitted yet.', $event)">
  Submit
</button>

<!-- using inline arrow function -->
<button @click="(event) => warn('Form cannot be submitted yet.', event)">
  Submit
</button>
```



```js
function warn(message, event) {
  // now we have access to the native event
  if (event) {
    event.preventDefault()
  }
  alert(message)
}
```


---

# See Code Example 2



---

## Binding HTML Classes


We can pass an object to `:class` (short for `v-bind:class`) to dynamically toggle classes:

```vue-html
<div :class="{ active: isActive }"></div>
```

The above syntax means the presence of the `active` class will be determined by the [truthiness](https://developer.mozilla.org/en-US/docs/Glossary/Truthy) of the data property `isActive`.

---

You can have multiple classes toggled by having more fields in the object. In addition, the `:class` directive can also co-exist with the plain `class` attribute. So given the following state:


```js
const isActive = ref(true)
const hasError = ref(false)
```



```js
data() {
  return {
    isActive: true,
    hasError: false
  }
}
```

And the following template:

```vue-html
<div
  class="static"
  :class="{ active: isActive, 'text-danger': hasError }"
></div>
```

It will render:

```vue-html
<div class="static active"></div>
```

When `isActive` or `hasError` changes, the class list will be updated accordingly. For example, if `hasError` becomes `true`, the class list will become `"static active text-danger"`.


---

# See Code Example 3

---

## `v-for` 

We can use the `v-for` directive to render a list of items based on an array. The `v-for` directive requires a special syntax in the form of `item in items`, where `items` is the source data array and `item` is an **alias** for the array element being iterated on:


```js
const items = ref([{ message: 'Foo' }, { message: 'Bar' }])
```


```vue-html
<li v-for="item in items">
  {{ item.message }}
</li>
```

---

Inside the `v-for` scope, template expressions have access to all parent scope properties. In addition, `v-for` also supports an optional second alias for the index of the current item:


```js
const parentMessage = ref('Parent')
const items = ref([{ message: 'Foo' }, { message: 'Bar' }])
```


```vue-html
<li v-for="(item, index) in items">
  {{ parentMessage }} - {{ index }} - {{ item.message }}
</li>
```


---

## Maintaining State with `key`

When Vue is updating a list of elements rendered with `v-for`, by default it uses an "in-place patch" strategy. If the order of the data items has changed, instead of moving the DOM elements to match the order of the items, Vue will patch each element in-place and make sure it reflects what should be rendered at that particular index.

This default mode is efficient, but **only suitable when your list render output does not rely on child component state or temporary DOM state (e.g. form input values)**.

To give Vue a hint so that it can track each node's identity, and thus reuse and reorder existing elements, you need to provide a unique `key` attribute for each item:

```vue-html
<div v-for="item in items" :key="item.id">
  <!-- content -->
</div>
```

---

[It is recommended](/style-guide/rules-essential#use-keyed-v-for) to provide a `key` attribute with `v-for` whenever possible, unless the iterated DOM content is simple (i.e. contains no components or stateful DOM elements), or you are intentionally relying on the default behavior for performance gains.

The `key` binding expects primitive values - i.e. strings and numbers. Do not use objects as `v-for` keys. For detailed usage of the `key` attribute, please see the [`key` API documentation](/api/built-in-special-attributes#key).



---

# See Code Example 4

---

# Event handling

We will directly read the official guide section on events : https://vuejs.org/guide/essentials/event-handling.html

---

# Form Input Bindings 



When dealing with forms on the frontend, we often need to sync the state of form input elements with corresponding state in JavaScript. It can be cumbersome to manually wire up value bindings and change event listeners:

```vue-html
<input
  :value="text"
  @input="event => text = event.target.value">
```

The `v-model` directive helps us simplify the above to:

```vue-html
<input v-model="text">
```

---


In addition, `v-model` can be used on inputs of different types, `<textarea>`, and `<select>` elements. It automatically expands to different DOM property and event pairs based on the element it is used on:

- `<input>` with text types and `<textarea>` elements use `value` property and `input` event;
- `<input type="checkbox">` and `<input type="radio">` use `checked` property and `change` event;
- `<select>` uses `value` as a prop and `change` as an event.

---

# See Code Example 5


---

# Creating sub components

See the section on Components Basics in the official guide : https://vuejs.org/guide/essentials/component-basics.html

Especially the first 2 points : 

- Defining a Component
- Using a Component

---

# See Code Example 6

In this example you may note :

- That we put each component in its own Vue file
- How we import a component from one source file to another
- How to insert children components into a parent component
  - Note the 2 possible case syntax : `PascalCase` (or `UpperCamelCase`) and  `kebab-case`
- Each component instance in the DOM has its own independant state

---

## Passing Props 

If we are building a blog, we will likely need a component representing a blog post. We want all the blog posts to share the same visual layout, but with different content. Such a component won't be useful unless you can pass data to it, such as the title and content of the specific post we want to display. That's where props come in.

Props are custom attributes you can register on a component. To pass a title to our blog post component, we must declare it in the list of props this component accepts, using the[`defineProps`](/api/sfc-script-setup#defineprops-defineemits) macro


```vue
<!-- BlogPost.vue -->
<script setup>
defineProps(['title'])
</script>

<template>
  <h4>{{ title }}</h4>
</template>
```

---

`defineProps` is a compile-time macro that is only available inside `<script setup>` and does not need to be explicitly imported. Declared props are automatically exposed to the template. 

`defineProps` also returns an object that contains all the props passed to the component, so that we can access them in JavaScript if needed:

```js
const props = defineProps(['title'])
console.log(props.title)
```

---

A component can have as many props as you like and, by default, any value can be passed to any prop.

Once a prop is registered, you can pass data to it as a custom attribute, like this:

```vue-html
<BlogPost title="My journey with Vue" />
<BlogPost title="Blogging with Vue" />
<BlogPost title="Why Vue is so fun" />
```


---

# See Code Example 7

---

## Emitting events (data)

See this guide section : https://vuejs.org/guide/essentials/component-basics.html#listening-to-events

---


# See Code Example 8

Once again, pay attention to the case convention for emits names. `camelCase` in JS code,
`kebab-case` in pseudo-HTML template. 

---

# Communicating with the backend

VueJS doesn't give you specific tools to communinate with the backend. Its role is 
to provide you with a way to bind data between JS and the DOM. In the JS code, 
that runs thanks to the VueJS framework, you are free to make AJAX like you did in
pure JS, and then interface the network requests with your code data.

In Code Example 9, we demonstrate how to display data from the server, in our 
Vue application.

---



# Chrome developper tools extension for VueJS

You can install this Chrome extension : https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd?hl=fr

It will give you a new tab in your chrome developpers tools, with practical tools 
to inspect the components of your Vue application.

![](./imgs/devtools.png)

---

# Assignment

With this short introduction to Vue, you should know all the funcitonalities needed
to work on the assignment.